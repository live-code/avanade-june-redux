import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { User } from '../../../model/user.ts';

// Define a service using a base URL and expected endpoints
type SmallUser = {id: number, title: string};

export const usersAPI = createApi({
  reducerPath: 'user',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://jsonplaceholder.typicode.com/' }),
  // NEW
  tagTypes: ['Users'],
  endpoints: (builder) => ({
    // query types: return type and query params <User, string>
    getUsers: builder.query<User[], void>({
      query: () => `users/`,
      providesTags: ['Users'],
    }),
    getUsersSmall: builder.query<SmallUser[], void>({
      query: () => `users`,
      transformResponse: (response: User[] ) => response.map(u => {
        return { id: u.id, title: u.name }
      }),
      providesTags: ['Users'],
      // limit cache to 5 sec (default is 60)
      keepUnusedDataFor: 2,
    }),
    getUserById: builder.query<User, string>({
      query: (id) => `users/${id}`,
    }),
    search: builder.query<User[], string>({
      query: (text) => `users?q=${text}`,
    }),
    deleteUser: builder.mutation({
      query: (id) => ({
        url: `users/${id}`,
        method: 'DELETE',
      }),
      // NEW
      invalidatesTags: ['Users'],
    }),
  }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
// export const { useGetPokemonByNameQuery } = userAPI
export const { usePrefetch, useDeleteUserMutation, useGetUsersQuery, useGetUsersSmallQuery, useSearchQuery, useGetUserByIdQuery } = usersAPI
