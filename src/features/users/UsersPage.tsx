
import { useDeleteUserMutation, useGetUsersSmallQuery } from './store/users.api';

export const UsersPage = () => {
  const { data, error, isLoading } = useGetUsersSmallQuery()
  // NEW
  const [
    removeUser,
    { isLoading: isLoadingRemove}
  ] = useDeleteUserMutation();

  return <div>

    <div>isLoading: {JSON.stringify(isLoading)}</div>
    <div>isLoadingRemove: {JSON.stringify(isLoadingRemove)}</div>
    <div>error: {JSON.stringify(error)}</div>
    <h1>data</h1>
    {
      data?.map((u) =>
        <li key={u.id}>
          {u.title}
          {/*NEW*/}
          <button onClick={(() => removeUser(u.id))}>-</button>
        </li>
      )
    }
    <hr/>
  </div>
};



/*
// get query
export const UsersPage = () => {
  const [name, setName] = useState<string>('Bret')
  const { data, error, isLoading } = useSearchQuery(name, {
    // invalid cache on mount
    //refetchOnMountOrArgChange: true
  })

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    setName(e.currentTarget.value)
  }

  return <div>

    <div>isLoading: {JSON.stringify(isLoading)}</div>
    <div>error: {JSON.stringify(error)}</div>
    <h1>data</h1>

    <input type="text" onChange={onChangeHandler} value={name}/>

    { data?.map((u) => <li key={u.id}>{u.name}</li>)}
    <hr/>

  </div>
};
*/

/*

// get
export const UsersPage = () => {

  const { data, isError, isLoading } = useGetUsersSmallQuery()

  return <div>
    UsersPage

    {isLoading && <div>loading....</div>}
    {isError && <div>ahia!</div>}
    {
      data?.map(u => <li key={u.id}>{u.name}</li>)
    }
  </div>
};
*/
