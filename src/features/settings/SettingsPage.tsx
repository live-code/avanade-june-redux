import { useDispatch } from 'react-redux';
import { changeLanguage, changeTheme } from '../../core/store/config.store.ts';

export const SettingsPage = () => {
  const dispatch = useDispatch()
  return <div>
    Settings

    <button onClick={() => dispatch(changeTheme('dark'))}>DARK</button>
    <button onClick={() => dispatch(changeTheme('light'))}>LIGHT</button>
    <button onClick={() => dispatch(changeLanguage('it'))}>IT</button>
    <button onClick={() => dispatch(changeLanguage('en'))}>EN</button>
  </div>
};


