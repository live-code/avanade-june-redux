import { combineReducers } from '@reduxjs/toolkit';
import { counterConfigReducer } from './counter-config/counter-config.reducer.ts';
import { counterReducer } from './counter/counter.reducer.ts';

export const counterReducers = combineReducers({
  value: counterReducer,
  config: counterConfigReducer
})
