import { createAction } from '@reduxjs/toolkit';
import { PalletMaterial } from '../../../../model/counter-config.ts';

export const updateMaterial = createAction<PalletMaterial>('update material')
export const updateItemsPerPallet = createAction<number>('update items per pallent')
