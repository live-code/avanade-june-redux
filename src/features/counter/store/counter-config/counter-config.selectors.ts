import { RootState } from '../../../../main.tsx';

export const selectMaterial = (state: RootState) => state.counter.config.material

export const selectItemsPerPallet = (state: RootState) => state.counter.config.itemsPerPallet;

export const selectTotalPallet = (state: RootState) => {
  return Math.ceil(state.counter.value/state.counter.config.itemsPerPallet)
}
