import { createReducer } from '@reduxjs/toolkit';
import { CounterConfig } from '../../../../model/counter-config.ts';
import { updateMaterial } from './counter-config.actions.ts';

const initialState: CounterConfig = {
  itemsPerPallet: 5,
  material: 'wood'
}
export const counterConfigReducer = createReducer<CounterConfig>(initialState, builder =>
  builder
    .addCase(updateMaterial, (state, action) => {
      state.material = action.payload

      if (action.payload === 'wood') {
        state.itemsPerPallet = 5;
      }
      if (action.payload === 'plastic') {
        state.itemsPerPallet = 10;
      }

    })
   /* .addCase(updateItemsPerPallet, (state, action) => {
      // state.itemsPerPallet = action.payload
      return { ...state, itemsPerPallet: action.payload}
    })*/
)
