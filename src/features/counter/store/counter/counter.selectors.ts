import { RootState } from '../../../../main.tsx';

export const selectCounter = (state: RootState) => state.counter.value;
