import { createReducer } from '@reduxjs/toolkit';
import { decrement, increment, reset } from './counter.actions.ts';

const initialState = 0;

export const counterReducer = createReducer(initialState, builder =>
  builder
    .addCase(increment, (state, action) => state + action.payload)
    .addCase(decrement, (state, action) => state - action.payload)
    .addCase(reset, () => initialState)
    //.addCase(changeTheme, () => initialState)

)

/*
// OLD STYLE TOOLKIT
export const counterReducer = createReducer(initialState, {
  [increment.type]: (state, action) => state + action.payload,
  [decrement.type]: (state, action) => state - action.payload,
  [reset.type]: () => initialState
})
*/

/*
// OLD STYLE
export function counterReducer(state = initialState, action: any) {
  console.log(state, action)
  switch (action.type) {
    case increment.type:
      return state + action.payload;

    case decrement.type:
      return state - action.payload;

    case reset.type:
      return initialState
  }
  return state
}
*/
