import { createAction } from '@reduxjs/toolkit';

// NEW STYLE (redux toolkit)
export const increment = createAction<number>('increment')
export const decrement = createAction<number>('decrement')
export const reset = createAction('reset')




// OLD STYLE (redux)
/*
export function increment(payload: number) {
  return { type: 'increment', payload }
}
*/
