import { useDispatch, useSelector } from 'react-redux';
import { updateMaterial } from './store/counter-config/counter-config.actions.ts';
import { selectItemsPerPallet, selectMaterial, selectTotalPallet } from './store/counter-config/counter-config.selectors.ts';
import { decrement, increment, reset } from './store/counter/counter.actions.ts';
import { selectCounter } from './store/counter/counter.selectors.ts';

export const CounterPage = () => {
  const dispatch = useDispatch();
  const total = useSelector(selectCounter)
  const material = useSelector(selectMaterial)
  const itemsPerPallet = useSelector(selectItemsPerPallet)
  const totalPallets = useSelector(selectTotalPallet)

  return <div>
    <h1>Total {total}</h1>
    <h2>Total Pallets: {totalPallets} ({itemsPerPallet} items per pallet) - {material}</h2>

    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(reset())}>reset</button>
    <hr/>
    <button onClick={() => dispatch(updateMaterial('wood'))}>wood (5)</button>
    <button onClick={() => dispatch(updateMaterial('plastic'))}>plastic (10)</button>

  </div>
};
