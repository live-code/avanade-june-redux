import * as classNames from 'classnames';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setFilter, VisibilityFilter } from './store/news-filters/news-filters.store.ts';
import { selectFilteredNews } from './store/news/news.selectors.ts';
import { add, remove, toggle } from './store/news/news.store.ts';



export const HomePage = () => {
  const news = useSelector(selectFilteredNews)
  const dispatch = useDispatch();

  function addNewsHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(add(e.currentTarget.value));
      e.currentTarget.value = '';
    }
  }

  function setVisibilityFilter(e: React.ChangeEvent<HTMLSelectElement>) {
    // dispatch(setVisibility(e.currentTarget.value as VisibilityFilter))
    dispatch(setFilter({
      visibility: e.currentTarget.value as VisibilityFilter
    }))
  }

  return <div>
    <h1>News list</h1>

    <input type="text" onKeyDown={addNewsHandler} placeholder="add news"/>
    <hr/>

    <select onChange={setVisibilityFilter} >
      <option value="all">show all</option>
      <option value="published">Published</option>
      <option value="unpublished">Draft</option>
    </select>

    <hr/>
    {
      news.map(item => {
        return <li key={item.id}>
          {item.title}
          <i className="fa fa-trash" onClick={() => dispatch(remove(item.id))} />
          <i
            className={classNames('fa', {
              'fa-eye': item.published,
              'fa-eye-slash': !item.published,
            } )}
            onClick={() => dispatch(toggle(item.id))}
          ></i>

        </li>
      })
    }
  </div>
};
