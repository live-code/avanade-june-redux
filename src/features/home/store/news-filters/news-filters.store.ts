import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type VisibilityFilter = 'all' | 'published' | 'unpublished'

export interface NewsFilterState {
  visibility: VisibilityFilter,
  text: string
}

const initialState: NewsFilterState = {
  visibility: 'all',
  text: ''
}

export const newsFiltersStore = createSlice({
  name: 'news',
  initialState,
  reducers: {
    setVisibility(state, action: PayloadAction<VisibilityFilter>) {
      state.visibility = action.payload
    },
    setFilter(state, action: PayloadAction<Partial<NewsFilterState>>) {
      // state = action.payload
      return { ...state, ...action.payload }
    }

  }
})

export const {setVisibility, setFilter} = newsFiltersStore.actions
