import { combineReducers } from '@reduxjs/toolkit';
import { newsFiltersStore } from './news-filters/news-filters.store.ts';
import { newsStore } from './news/news.store.ts';

export const newsReducers = combineReducers({
  list: newsStore.reducer,
  filters: newsFiltersStore.reducer
})
