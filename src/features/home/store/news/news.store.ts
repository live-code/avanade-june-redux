import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { News } from '../../../../model/news.ts';


const initialState: News[] = [
  { id: 1, title: 'news 1', published: false },
  { id: 2, title: 'news 2', published: true },
  { id: 3, title: 'news 3', published: false },
];

export const newsStore = createSlice({
  name: 'news',
  initialState,
  reducers: {
    add(state, action: PayloadAction<string>) {
      const news: News = {
        title: action.payload,
        published: false,
        id: Date.now()
      }
      // state.push(news)
      return [...state, news]
    },
    remove(state, action: PayloadAction<number>) {
      // return state.filter(item => item.id !== action.payload)
      const index = state.findIndex(item => item.id === action.payload)
      state.splice(index, 1)
    },
    toggle(state, action: PayloadAction<number>) {
      const news = state.find(item => item.id === action.payload)
      if (news) {
        news.published = !news.published
      }
    }
  },
  /*extraReducers: builder => builder
    .addCase(changeTheme, (state, action) => {
      return []
    })*/
})

export const { add, remove,toggle } = newsStore.actions
