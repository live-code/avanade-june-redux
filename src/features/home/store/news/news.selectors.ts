import { RootState } from '../../../../main.tsx';

export const selectFilteredNews = (state: RootState) => {
  switch (state.news.filters.visibility) {
    case 'published':
      return state.news.list.filter(item => item.published);
    case 'unpublished':
      return state.news.list.filter(item => !item.published);
    default:
      return state.news.list;
  }
}
