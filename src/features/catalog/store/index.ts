import { combineReducers } from '@reduxjs/toolkit';
import { productsStore } from './products/products.store.ts';

export const catalogReducers = combineReducers({
  products: productsStore.reducer
})
