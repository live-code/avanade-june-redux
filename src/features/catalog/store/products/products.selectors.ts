import { RootState } from '../../../../main.tsx';

export const selectProductsList = (state: RootState) => state.catalog.products.list
export const selectProductsTotal = (state: RootState) =>
  state.catalog.products.list.reduce((acc, item) => {
    return acc + item.price
  }, 0)
