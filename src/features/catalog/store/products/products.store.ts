import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Product } from '../../../../model/product.ts';


interface ProductState {
  list: Product[],
  error: null | string;
  loading: boolean;
}

const initialState: ProductState = {
  list: [],
  error: null,
  loading: false
}


export const productsStore = createSlice({
  name: 'products',
  initialState,
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      return {
        error: null,
        loading: false,
        list: action.payload
      }
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.loading = false;
      state.error = null;
      state.list.push(action.payload)
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      state.loading = false;
      state.error = null;
      const index = state.list.findIndex(p => p.id === action.payload)
      state.list.splice(index, 1)
    },
    toggleProductVisibility(state, action: PayloadAction<number>) {
      state.loading = false;
      state.error = null;
      const product = state.list.find(p => p.id === action.payload);
      if (product) {
        product.visibility = !product.visibility
      }
    },
    showLoading(state) {
      state.loading = true
    },
    hideLoading(state) {
      state.loading = false
    },
  /*  getProductsFail(state, action: PayloadAction<string>) {
     state.error = action.payload
    },
    deleteProductsFail(state, action: PayloadAction<string>) {
      state.error = action.payload
    },*/
    setError(state, action: PayloadAction<string>) {
      state.loading = false;
      state.error = action.payload
    },
  }
});

export const {
  addProductSuccess, getProductsSuccess, toggleProductVisibility, deleteProductSuccess,
  setError, showLoading, hideLoading
} = productsStore.actions;
