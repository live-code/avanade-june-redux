import axios from 'axios';
import { AppThunk } from '../../../../main.tsx';
import { Product } from '../../../../model/product.ts';
import {
  addProductSuccess,
  deleteProductSuccess,
  getProductsSuccess, setError, showLoading
} from './products.store.ts';



export const getProducts = (): AppThunk => async (dispatch) => {
  dispatch({ type: 'getProducts' });
  dispatch(showLoading());
  try {
    const res = await axios.get('http://localhost:3000/products')
    dispatch(getProductsSuccess(res.data))
  } catch(e) {
    dispatch(setError('products not loaded '))
  }
}


export const deleteProduct = (id: number): AppThunk => async (dispatch) => {
  dispatch({ type: 'deleteProduct', payload: id  })
  try {
    await axios.delete(`http://localhost:3000/products/${id}`)
    dispatch(deleteProductSuccess(id))
  } catch(e) {
    dispatch(setError('delete fails'))
  }
}


export const addProduct = (
  product: Pick<Product, 'title' | 'price'>
): AppThunk => async (dispatch) => {
  dispatch({ type: 'addProduct', payload: product })

  const newProduct = {...product, visibility: false}

  try {
    const res = await axios.post<Product>(`http://localhost:3000/products`, newProduct)
    dispatch(addProductSuccess(res.data))
  } catch(e) {
    // ...
  }
}

export function getProductsOLD(): AppThunk {
  return function(dispatch) {
    dispatch({ type: 'getProducts' })
    axios.get('http://localhost:3000/products')
      .then(res => {
        dispatch(getProductsSuccess(res.data))
      })
      .catch()
  }
}
