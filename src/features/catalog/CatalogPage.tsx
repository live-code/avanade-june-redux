import React, { useEffect } from 'react';
import { Root } from 'react-dom/client';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from '../../main.tsx';
import { Product } from '../../model/product.ts';
import { addProduct, deleteProduct, getProducts } from './store/products/products.actions.ts';
import { selectProductsList } from './store/products/products.selectors.ts';

export const CatalogPage: React.FC<any> = () => {
  const products = useSelector(selectProductsList);
  const error = useSelector((state: RootState) => state.catalog.products.error)
  const loading = useSelector((state: RootState) => state.catalog.products.loading)
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);


  function addTodoHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addProduct({
        title: e.currentTarget.value,
        price: 10,
      }))
      e.currentTarget.value = '';
    }
  }

  return <div>
    <div>PRODUCT: {products.length}</div>

    {loading && <div>Loading...</div>}
    {error && <div className="alert alert-danger">{error}</div>}

    <hr/>
    <input type="text" onKeyDown={addTodoHandler}/>

    {
      products.map((product: Product) => {
        return (
          <li
            className="list-group-item"
            key={product.id}
          >
            <span className="ml-2">{ product.title } € {product.price}</span>

            <i
              onClick={() => dispatch(deleteProduct(product.id))}
              className="fa fa-trash"></i>
          </li>
        )
      })
    }
  </div>
};
