import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Config, Language, Theme } from '../../model/config.ts';

const initialState: Config = {
  theme: 'dark',
  language: 'it',
};

export const configStore = createSlice({
  name: 'config',
  initialState,
  reducers: {
    changeTheme(state, action: PayloadAction<Theme>) {
      state.theme = action.payload;
    },
    changeLanguage(state, action: PayloadAction<Language>) {
      state.language = action.payload;
    },
    updateConfig(state, action: PayloadAction<Partial<Config>>) {
      return {
        ...state,
        ...action.payload
      }
    }
  }
})

export const {
  changeLanguage,
  changeTheme,
  updateConfig
} = configStore.actions
