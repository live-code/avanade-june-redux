import * as classNames from 'classnames';
import React, { memo } from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RootState } from '../../main.tsx';

const activeStyle: React.CSSProperties = { color: 'orange'};

export const Navbar = () => {

  const theme = useSelector((state: RootState) => state.config.theme)
  const language = useSelector((state: RootState) => state.config.language)

  console.log('render navbar')
  return (
    <nav className={classNames(
      'navbar navbar-expand',
      {
        'navbar-light bg-light': theme === 'light',
        'navbar-dark bg-dark': theme === 'dark',
      }
    )}>
      <div className="navbar-brand">
        <NavLink
          className="nav-link"
          to="/">
          REDUX
          {
            language === 'it' ? '🇮🇹' : '🇬🇧'
          }
        </NavLink>
      </div>

      <MyComponent ></MyComponent>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink style={(obj) => obj.isActive ? activeStyle  : {} }
                     className="nav-link"
                     to="/settings">
              <small>settings</small>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/counter">
              <small>counter</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/users">
              <small>users</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/catalog">
              <small>catalog</small>
            </NavLink>
          </li>

        </ul>
      </div>
    </nav>
  )
}


const MyComponent = memo(() => {
  console.log('  render MyCOmponent')
  const theme = useSelector((state: RootState) => state.config.theme)

  return <div>⚡️️️️️️️️️️️️♥️ {theme}</div>
})
