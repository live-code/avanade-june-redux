import { AnyAction, combineReducers, configureStore, ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import ReactDOM  from 'react-dom/client'
import { Provider, useDispatch } from 'react-redux';
import App from './App.tsx'
import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import { configStore } from './core/store/config.store.ts';
import { catalogReducers } from './features/catalog/store';
import { productsStore } from './features/catalog/store/products/products.store.ts';
import { counterReducers } from './features/counter/store';
import { newsReducers } from './features/home/store';
import { usersAPI } from './features/users/store/users.api.ts';

const rootReducer = combineReducers({
  counter: counterReducers,
  todos: () => [1, 2, 3],
  config: configStore.reducer,
  news: newsReducers,
  catalog: catalogReducers,
  [usersAPI.reducerPath]: usersAPI.reducer
})

export const store = configureStore({
  reducer: rootReducer,
  devTools: import.meta.env.DEV,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(usersAPI.middleware),

})

export type RootState = ReturnType<typeof rootReducer>
export type AppThunk = ThunkAction<void, RootState, any, AnyAction>;
export type AppDispatch = ThunkDispatch<RootState, any, AnyAction>;
export const useAppDispatch = () => useDispatch<AppDispatch>()

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
    <App />
  </Provider>
)
