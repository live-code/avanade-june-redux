import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { Navbar } from './core/components/Navbar.tsx';
import { CatalogPage } from './features/catalog/CatalogPage.tsx';
import { CounterPage } from './features/counter/CounterPage.tsx';
import { HomePage } from './features/home/HomePage.tsx';
import { SettingsPage } from './features/settings/SettingsPage.tsx';
import { UsersPage } from './features/users/UsersPage.tsx';




function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/home" element={<HomePage />} />
        <Route path="/counter" element={<CounterPage />} />
        <Route path="/users" element={<UsersPage />} />
        <Route path="/catalog" element={<CatalogPage />} />
        <Route path="/settings" element={<SettingsPage />} />
        <Route
          path="*"
          element={
            <Navigate to='/home' />
          }
        />
      </Routes>
    </BrowserRouter>
  )
}

export default App
