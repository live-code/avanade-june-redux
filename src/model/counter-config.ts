export type PalletMaterial = 'wood' | 'plastic';

export interface CounterConfig {
  itemsPerPallet: number;
  material: PalletMaterial
}
